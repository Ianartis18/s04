from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod

	def __init__(self):
		pass

	def eat(self,food):
		self._food = food
		pass


	def make_sound(self):
		pass	
	

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def eat(self, food):
		self._food = food
		print(f"Serve me {self._food}")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self._name}, Come on!")

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def eat(self, food):
		self._food = food
		print(f"Eaten {self._food}")

	def make_sound(self):
		print("Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self._name}!")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()
cat1 = Cat("Puss", "Persian", 4)
dog1.eat("Tuna")
cat1.make_sound()
cat1.call()

